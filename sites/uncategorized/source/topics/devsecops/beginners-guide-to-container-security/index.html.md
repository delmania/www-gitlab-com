---
layout: markdown_page
title: "A beginner’s guide to container security"
description: DevOps teams utilize containers more than ever today but securing them is often an afterthought. Here’s what you need to know about container security.
---

Resource efficient and highly portable containers are increasingly the go-to choice for modern software development. In fact, by 2023 more than 70% of organizations will be running more than two containerized applications, according to market research firm Gartner.

But containers have their downsides, particularly when it comes to security. GitLab’s [2020 Global DevSecOps Survey](/developer-survey/) found 56% of developers simply don’t run container scans, and a majority of [DevOps teams](/topics/devops/) don’t have a security plan in place for containers or many other cutting edge software technologies, including cloud native/serverless, APIs, and microservices.

The solution is for DevOps teams to shift left and integrate security practices into each stage of the application lifecycle. Here’s a step-by-step look at how teams can accomplish that goal.

## Understand the problem

Container security differs from traditional security methods due to the increased complexity and dynamism of the container environment. Simply put, there are a lot more moving pieces. Container security comprises everything from the applications they contain to the infrastructure they run on. Base image security and quality are critical to ensure that any derivative images come from a trusted source. Build security in your container pipeline by gathering trusted images, managing access with the use of a private registry, integrating security testing and automating deployment, and continuously defending your infrastructure.

While containers seem to behave like small <a href="https://searchservervirtualization.techtarget.com/definition/virtual-machine/" target="_blank">virtual machines (VMs)</a> , they actually don’t – and so require a different security strategy. Traffic between apps in a container does not cross perimeter network security, but should be monitored for malicious traffic between apps and their images. Your orchestrator can be used to set security policies for processes and resources, but a complete security strategy requires more.

## Many layers = many needs

Container images define what runs in each container. Developers should make sure that images don’t contain any vulnerabilities, and should avoid creating extraneous images to minimize the container’s attack surface. Image validation tooling can be used to forbid untrusted images, but is often not enabled by default. Images can also be scanned after they’re built to detect dependent images that might also have vulnerabilities.

Unlike VMs, multiple containers can run on the same operating system and an attack can happen at either level. A vulnerable host OS puts its containers at risk, and a vulnerable container can open an attack pathway to the host OS. Enforce <a href="https://gardener.cloud/documentation/guides/applications/network-isolation/" target="_blank">namespace isolation</a> to limit interaction between container and host OS kernel, and automating patching to align with vendor patch releases. The OS should also be as simple as possible, free from unnecessary components (such as apps or libraries that aren’t actually needed to run your orchestrator).

Container orchestration coordinates and manages containers, allowing containerized applications to scale and support thousands of users. Your orchestrator may have its own out-of-the-box security features, possibly allowing you to create rules for Kubernetes to automatically enforce across all pods within the cluster. This type of basic feature is useful, but is also only a first step towards a more robust set of policies.

## Smart secret storage

Containers may be spread across multiple systems and cloud providers, making access management all the more important. Secrets, which include API keys, login credentials, and tokens, should be stringently managed to ensure container access is limited to privileged users. User access can also be defined by role-based access control, allowing you to limit access to an as-needed basis.

## The importance of runtime security

Once deployed, container activity must be monitored and teams need to be able to detect and respond to any security threats. Nordcloud suggests monitoring for suspicious behaviors such as network calls, API calls, and unusual login attempts. Teams should have predefined mitigation steps for pods, and should be able to isolate the container on a different network, restart it, or stop it until the threat is identified.

## Cloud native requires greater accountability

Cloud providers also take on some of the work by securing the underlying hardware and basic networks used to provide cloud services, but users still share that responsibility, as well as assuming responsibility for application access, app configuration and patching, and system patching and access.

## The NIST guidelines for container security

In 2017, the U.S. Department of Commerce published its <a href="https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-190.pdf" target="_blank">Application Container Security Guide</a>. Although this guide is a few years old, teams just adopting containers can still benefit from the following recommendations: 

“Tailor the organization’s operational culture and technical processes to support the new way of developing, running, and supporting applications made possible by containers.”
Adopting containers might be disruptive to your existing culture and development methodologies, and your current practices might not be directly applicable in a containerized environment. Encourage, educate, and train your team to rethink how they code and operate.

“Use container-specific host OSs instead of general-purpose ones to reduce attack surfaces.”
A container-specific host OS is a minimalist OS designed to only run containers. Using these OSs greatly reduces attack surfaces, allowing fewer opportunities for your containers to be compromised.

“Only group containers with the same purpose, sensitivity, and threat posture on a single host OS kernel to allow for additional in-depth defense.”
Segmenting containers provides additional defense in-depth. Grouping containers in this manner makes it more difficult for an attacker to expand potential compromises to other groups. It also increases the likelihood that compromises will be detected and contained.

“Adopt container-specific vulnerability management tools and processes for images to prevent compromises.”
Traditional tools make many assumptions that are misaligned with a containerized model, and are often unable to detect vulnerabilities within containers. Organizations should adopt tools and processes to validate and enforce compliance with secure configuration best practices for images – including centralized reporting, monitoring each image, and preventing non-compliant images from being run.

“Consider using hardware-based countermeasures to provide a basis for trusted computing.”
Extend security practices across all tiers of the container technology by basing security on a hardware root of trust, such as the <a href="https://whatis.techtarget.com/definition/trusted-platform-module-TPM" target="_blank">.

“Use container-aware runtime defense tools.”
Deploy and use a dedicated container security solution capable of monitoring the container environment and providing precise detection of anomalous and malicious activity within it.

## Learn more about container security:

[Learn the lingo](/blog/2020/07/30/kubernetes-terminology/) and get hands on

Dive into [DevsecOps security basics](/topics/devsecops/) 

Get started with [DevSecOps with GitLab](/solutions/dev-sec-ops/)

